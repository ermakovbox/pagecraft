using Avalonia.Controls;

namespace PageCraft.Resources.Interfaces;

public interface IUiCanvasManager
{ 
    ComboBox CreateShapeComboBox();
}