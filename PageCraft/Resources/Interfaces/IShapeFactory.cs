using Avalonia.Controls;

namespace PageCraft.Resources.Interfaces;

public interface IShapeFactory
{
    Control CreateShape(string shapeType);
}