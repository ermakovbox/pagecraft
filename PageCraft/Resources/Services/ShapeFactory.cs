using System;
using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Media;
using PageCraft.Resources.Interfaces;

namespace PageCraft.Resources.Services;

public class ShapeFactory : IShapeFactory
{
    public Control CreateShape(string shapeType)
    {
        return shapeType switch
        {
            nameof(Rectangle) => CreateRectangle(),
            nameof(Ellipse) => CreateEllipse(),
            nameof(Line) => CreateLine(),
            nameof(Polygon) => CreatePolygon(),
            _ => throw new ArgumentException("Unknown shape type", nameof(shapeType))
        };
    }

    private static Rectangle CreateRectangle()
    {
        var rectangle = new Rectangle
        {
            Width = 100,
            Height = 50,
            Fill = Brushes.Blue,
            StrokeThickness = 5
        };
        return rectangle;
    }

    private static Ellipse CreateEllipse()
    {
        var ellipse = new Ellipse
        {
            Width = 50,
            Height = 50,
            Fill = Brushes.Red,
            StrokeThickness = 5
        };
        return ellipse;
    }

    private static Line CreateLine()
    {
        var line = new Line
        {
            StartPoint = new Point(0, 0),
            EndPoint = new Point(100, 100),
            Stroke = Brushes.Orange,
            StrokeThickness = 3
        };
        return line;
    }
    
    private static Polygon CreatePolygon()
    {
        var polygon = new Polygon
        {
            Points = new List<Point>
            {
                new (0, 0),
                new (100, 0),
                new (100, 100),
                new (0, 100)
            },
            Fill = Brushes.Green,
            Stroke = Brushes.Black,
            StrokeThickness = 3
        };
        return polygon;
    }
}