using System;
using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Input;
using Avalonia.Media;
using PageCraft.Resources.Interfaces;

namespace PageCraft.Resources.Services;

public class UiController
{ 
    private bool _isMoving;
    private bool _isResizing;
    private Point _startPoint;
    private string? _resizeDirection;
    public Control CreateAndConfigureShape(string shapeType)
    {
        IShapeFactory shapeFactory = new ShapeFactory();
        var shape = shapeFactory.CreateShape(shapeType);
        ConfigureShapeEvents(shape);
        return shape;
    }

    private void ConfigureShapeEvents(InputElement shape)
    {
        shape.PointerPressed += Object_PointerPressed;
        shape.PointerMoved += Object_PointerMoved;
        shape.PointerReleased += Object_PointerReleased;
        shape.DoubleTapped += Object_PointerDoubleClick;
    }

    private void Object_PointerPressed(object? sender, PointerPressedEventArgs e)
    {
        var selectedTypeObj = sender as Control;
            
        switch (selectedTypeObj)
        {
            case Rectangle selectedRectangle:
                if (!CanvasManager.SelectedObjects.Contains(selectedRectangle)) 
                    CanvasManager.SelectedObjects.Add(selectedRectangle);
                selectedRectangle.Stroke = Brushes.DarkCyan;
                SetupResizeHandles(selectedRectangle);
                break;
            case Ellipse selectedEllipse:
                if (!CanvasManager.SelectedObjects.Contains(selectedEllipse)) 
                    CanvasManager.SelectedObjects.Add(selectedEllipse);
                selectedEllipse.Stroke = Brushes.DarkCyan;
                SetupResizeHandles(selectedEllipse);
                break;
            case Line selectedLine:
                if (!CanvasManager.SelectedObjects.Contains(selectedLine)) 
                    CanvasManager.SelectedObjects.Add(selectedLine);
                selectedLine.Stroke = Brushes.DarkCyan;
                SetupResizeHandles(selectedLine);
                break;
            case Polygon selectedPolygon:
                if (!CanvasManager.SelectedObjects.Contains(selectedPolygon)) 
                    CanvasManager.SelectedObjects.Add(selectedPolygon);
                selectedPolygon.Stroke = Brushes.DarkCyan;
                SetupResizeHandles(selectedPolygon);
                break;
        }
            
        _isMoving = CanvasManager.SelectedObjects.Count > 0;
        _isResizing = CanvasManager.SelectedObjects.Count == 1;
        _startPoint = e.GetPosition(CanvasManager.Canvas);
            
        foreach (var obg in CanvasManager.SelectedObjects)
        {
            obg.Tag = true;
        }
    }
    
    private void Object_PointerMoved(object? sender, PointerEventArgs e)
    {
        if (_isMoving)
        {
            var position = e.GetPosition(CanvasManager.Canvas);
            var deltaX = position.X - _startPoint.X;
            var deltaY = position.Y - _startPoint.Y;
            
            foreach (var rectangle in CanvasManager.SelectedObjects)
            {
                if (rectangle.Tag is bool isDragging && isDragging)
                {
                    Canvas.SetLeft(rectangle, Canvas.GetLeft(rectangle) + deltaX);
                    Canvas.SetTop(rectangle, Canvas.GetTop(rectangle) + deltaY);
                        
                    // Перемещаем ручки изменения размера вместе с объектом
                    MoveResizeHandles(rectangle, deltaX, deltaY);
                }
            }
            
            _startPoint = position;
        }
    }
    
    private void Object_PointerDoubleClick(object? sender, TappedEventArgs e)
    {
        var doubleClickedObj = sender as Control;
        if (doubleClickedObj is null) return;
            
        switch (doubleClickedObj)
        {
            case Rectangle rectangle:
                rectangle.Stroke = Brushes.Transparent;
                CanvasManager.SelectedObjects.Remove(doubleClickedObj);
                break;
            case Ellipse ellipse:
                ellipse.Stroke = Brushes.Transparent;
                CanvasManager.SelectedObjects.Remove(doubleClickedObj);
                break;
            case Line line:
                line.Stroke = Brushes.Transparent;
                CanvasManager.SelectedObjects.Remove(doubleClickedObj);
                break;
        }

        // Удаляем ручки изменения размеровё
        var resizeHandles = CanvasManager.Canvas.Children.OfType<Ellipse>().Where(x => Equals(x.Tag, doubleClickedObj)).ToList();
        foreach (var resizeHandle in resizeHandles)
        {
            CanvasManager.Canvas.Children.Remove(resizeHandle);
        }
    }
    
    private void Object_PointerReleased(object? sender, PointerReleasedEventArgs e)
    {
        _isMoving = false;
        _isResizing = false;
            
        foreach (var rectangle in CanvasManager.SelectedObjects)
        {
            rectangle.Tag = false;
        }
    }

    private void SetupResizeHandles(Control obj)
    {
        var resizeHandles = new List<Ellipse>();

        // Создаем ручки для изменения размеров
        for (var i = 0; i < 4; i++)
        {
            var resizeHandle = new Ellipse
            {
                Width = 20,
                Height = 20,
                Fill = Brushes.DarkCyan, // Цвет ручки можно изменить
                Name = GetResizeHandleName(i), // Метод, чтобы получить уникальное имя для каждой ручки
                Cursor = GetCursorForResizeHandle(i), // Метод, чтобы установить курсор в зависимости от направления изменения размеров
                Tag = obj, // Связываем ручку с прямоугольником
                ZIndex = 15
            };

            // Позиционируем ручки в каждом углу прямоугольника
            double handleLeft = 0, handleTop = 0;

            switch (i)
            {
                case 0: // Верхний левый угол
                    handleLeft = Canvas.GetLeft(obj) - resizeHandle.Width / 2;
                    handleTop = Canvas.GetTop(obj) - resizeHandle.Height / 2;
                    break;
                case 1: // Верхний правый угол
                    handleLeft = Canvas.GetLeft(obj) + obj.Width - resizeHandle.Width / 2;
                    handleTop = Canvas.GetTop(obj) - resizeHandle.Height / 2;
                    break;
                case 2: // Нижний правый угол
                    handleLeft = Canvas.GetLeft(obj) + obj.Width - resizeHandle.Width / 2;
                    handleTop = Canvas.GetTop(obj) + obj.Height - resizeHandle.Height / 2;
                    break;
                case 3: // Нижний левый угол
                    handleLeft = Canvas.GetLeft(obj) - resizeHandle.Width / 2;
                    handleTop = Canvas.GetTop(obj) + obj.Height - resizeHandle.Height / 2;
                    break;
            }

            Canvas.SetLeft(resizeHandle, handleLeft);
            Canvas.SetTop(resizeHandle, handleTop);

            // Добавляем обработчики событий
            resizeHandle.PointerPressed += ResizeHandle_PointerPressed;
            resizeHandle.PointerMoved += ObjectResize_PointerMoved;

            resizeHandles.Add(resizeHandle);
        }

        // Добавляем ручки на Canvas...
        foreach (var resizeHandle in resizeHandles) 
            CanvasManager.Canvas.Children.Add(resizeHandle);

        // Обновляем позиции ручек изменения размера
        UpdateResizeHandlesPositions();
    }

    private string GetResizeHandleName(int index)
    {
        // Генерируем уникальное имя для ручки в зависимости от индекса
        return $"ResizeHandle_{index}";
    }

    private Cursor GetCursorForResizeHandle(int index)
    {
        // Устанавливаем курсор в зависимости от направления изменения размеров
        switch (index)
        {
            case 0: // Верхний левый угол
            case 2: // Нижний правый угол
                return new Cursor(StandardCursorType.TopLeftCorner);
            case 1: // Верхний правый угол
            case 3: // Нижний левый угол
                return new Cursor(StandardCursorType.TopRightCorner);
            default:
                return new Cursor(StandardCursorType.Arrow);
        }
    }

    private void MoveResizeHandles(object obj, double deltaX, double deltaY)
    {
        // Найти все ручки изменения размера, связанные с данным прямоугольником
        var resizeHandles = CanvasManager.Canvas.Children.OfType<Ellipse>().Where(x => Equals(x.Tag, obj)).ToList();

        foreach (var resizeHandle in resizeHandles)
        {
            Canvas.SetLeft(resizeHandle, Canvas.GetLeft(resizeHandle) + deltaX);
            Canvas.SetTop(resizeHandle, Canvas.GetTop(resizeHandle) + deltaY);
        }
    }

    private void UpdateResizeHandlesPositions()
    {
        foreach (var rectangle in CanvasManager.SelectedObjects)
        {
            var resizeHandles = CanvasManager.Canvas.Children.OfType<Ellipse>().Where(x => Equals(x.Tag, rectangle))
                .ToList();

            foreach (var resizeHandle in resizeHandles)
            {
                // Позиционируем ручки в каждом углу прямоугольника
                double handleLeft = 0, handleTop = 0;

                switch (resizeHandle.Name)
                {
                    case "ResizeHandle_0": // Верхний левый угол
                        handleLeft = Canvas.GetLeft(rectangle) - resizeHandle.Width / 2;
                        handleTop = Canvas.GetTop(rectangle) - resizeHandle.Height / 2;
                        break;
                    case "ResizeHandle_1": // Верхний правый угол
                        handleLeft = Canvas.GetLeft(rectangle) + rectangle.Width - resizeHandle.Width / 2;
                        handleTop = Canvas.GetTop(rectangle) - resizeHandle.Height / 2;
                        break;
                    case "ResizeHandle_2": // Нижний правый угол
                        handleLeft = Canvas.GetLeft(rectangle) + rectangle.Width - resizeHandle.Width / 2;
                        handleTop = Canvas.GetTop(rectangle) + rectangle.Height - resizeHandle.Height / 2;
                        break;
                    case "ResizeHandle_3": // Нижний левый угол
                        handleLeft = Canvas.GetLeft(rectangle) - resizeHandle.Width / 2;
                        handleTop = Canvas.GetTop(rectangle) + rectangle.Height - resizeHandle.Height / 2;
                        break;
                }

                Canvas.SetLeft(resizeHandle, handleLeft);
                Canvas.SetTop(resizeHandle, handleTop);
            }
        }
    }

    private void ResizeHandle_PointerPressed(object? sender, PointerPressedEventArgs e)
    {
        if (sender is not Ellipse resizeHandle) return;

        _isResizing = true;
        _isMoving = false;
        _resizeDirection = resizeHandle.Name;

        _startPoint = e.GetPosition(CanvasManager.Canvas);

        foreach (var rectangle in CanvasManager.SelectedObjects) rectangle.Tag = true;
    }

    private void ObjectResize_PointerMoved(object? sender, PointerEventArgs e)
    {
        if (_isResizing && !string.IsNullOrWhiteSpace(_resizeDirection))
        {
            var position = e.GetPosition(CanvasManager.Canvas);

            foreach (var rectangle in CanvasManager.SelectedObjects)
            {
                if (!e.GetCurrentPoint(rectangle).Properties.IsLeftButtonPressed) return;
                if (rectangle.Tag is bool isDragging and true)
                {
                    var deltaX = position.X - _startPoint.X;
                    var deltaY = position.Y - _startPoint.Y;

                    double newWidth = rectangle.Width;
                    double newHeight = rectangle.Height;

                    if (_resizeDirection.Contains("ResizeHandle_0"))
                    {
                        newWidth -= deltaX;
                        Canvas.SetLeft(rectangle, Math.Round(Canvas.GetLeft(rectangle) + deltaX, 0));
                        newHeight -= deltaY;
                        Canvas.SetTop(rectangle, Math.Round(Canvas.GetTop(rectangle) + deltaY, 0));
                    }
                    else if (_resizeDirection.Contains("ResizeHandle_1"))
                    {
                        newWidth += deltaX;
                        newHeight -= deltaY;
                        Canvas.SetTop(rectangle, Math.Round(Canvas.GetTop(rectangle) + deltaY, 0));
                    }
                    else if (_resizeDirection.Contains("ResizeHandle_2"))
                    {
                        newWidth += deltaX;
                        newHeight += deltaY;
                    }
                    else if (_resizeDirection.Contains("ResizeHandle_3"))
                    {
                        newWidth -= deltaX;
                        Canvas.SetLeft(rectangle, Math.Round(Canvas.GetLeft(rectangle) + deltaX, 0));
                        newHeight += deltaY;
                    }


                    rectangle.Width = Math.Round(newWidth, 0);
                    rectangle.Height = Math.Round(newHeight, 0);

                    // Обновляем позиции ручек изменения размера
                    UpdateResizeHandlesPositions();
                }
            }

            _startPoint = position;
        }
    }
}