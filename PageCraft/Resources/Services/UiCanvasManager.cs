using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using PageCraft.Resources.Interfaces;

namespace PageCraft.Resources.Services;

public class UiCanvasManager : IUiCanvasManager
{
    public ComboBox CreateShapeComboBox()
    {
        var shapeComboBox = new ComboBox
        {
            Items = { nameof(Rectangle), nameof(Ellipse), nameof(Line), nameof(Polygon) },
            SelectedIndex = 0
        };
        
        shapeComboBox.SelectionChanged += ShapeComboBox_SelectionChanged;
        return shapeComboBox;
    }

    private static void ShapeComboBox_SelectionChanged(object? sender, SelectionChangedEventArgs e)
    {
        // Получаем выбранную фигуру
        var shapeComboBox = sender as ComboBox;
        if (shapeComboBox?.SelectedItem is not string selectedShape) return;
        
        var uiController = new UiController();
        var shape = uiController.CreateAndConfigureShape(selectedShape);
        
        CanvasManager.CurrentObject = shape;
    }
    
}