namespace PageCraft.Resources.Services;

public class WindowService
{
    public void ShowMainWindow()
    {
        var mainWindow = new MainWindow();
        mainWindow.Show();
    }

    public void ShowPanAndZoomWindow()
    {
        var panAndZoomWindow = new PanAndZoomWindow();
        panAndZoomWindow.Show();
    }
}