using System;
using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.PanAndZoom;
using Avalonia.Controls.Primitives;
using Avalonia.Input;
using Avalonia.Layout;
using Avalonia.Media;
using PageCraft.Resources.Interfaces;

namespace PageCraft.Resources.Services;

public class CanvasManager
{
    private readonly StackPanel _stackPanel;
    private readonly IUiCanvasManager _uiCanvasManager;
    public static Canvas Canvas { get; private set; } = null!;
    public static Control? CurrentObject { get; set; }
    public static List<Control> SelectedObjects { get; } = new();
    private double _currentZoom = 1.0;

    public CanvasManager(StackPanel stackPanel, string canvasName, int canvasWidth, int canvasHeight,
        IUiCanvasManager uiCanvasManager)
    {
        _stackPanel = stackPanel;
        _uiCanvasManager = uiCanvasManager;
        Canvas = InitializeCanvas(canvasName, canvasWidth, canvasHeight);
        _stackPanel = InitializeStackPanel(_stackPanel, Canvas);
        
        var mouseCoordinatesLabel = new TextBlock
        {
            Foreground = Brushes.Black,
            Margin = new Thickness(5),
        };
        _stackPanel.Children.Add(mouseCoordinatesLabel);

    }
    
    private Canvas InitializeCanvas(string canvasName, int canvasWidth, int canvasHeight)
    {
        var canvas = new Canvas
        {
            Name = canvasName,
            Width = canvasWidth,
            Height = canvasHeight,
            Background = Brushes.LightGray,
        };
        
        canvas.PointerPressed += Canvas_PointerPressed;

        return canvas;
    }
    
    private StackPanel InitializeStackPanel(StackPanel stackPanel, Control canvas)
    {
        var scrollViewer = new ScrollViewer()
        {
            Name = "ScrollViewer",
            Width = canvas.Width,
            Height = canvas.Height,
            HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
            VerticalScrollBarVisibility = ScrollBarVisibility.Auto
        };
        
        var zoomBorder = new ZoomBorder
        {
            Name = "ZoomBorder",
            Child = canvas,
            Width = canvas.Width,
            Height = canvas.Height,
            Background = Brushes.SlateBlue,
            HorizontalAlignment = HorizontalAlignment.Stretch,
            VerticalAlignment = VerticalAlignment.Stretch,
            ClipToBounds = true,
            Focusable = true,
            ZoomSpeed = 1.2,
            Stretch = StretchMode.None
        };
        
        scrollViewer.Content = zoomBorder; // добавили в ScrollViewer ZoomBorder, а в ZoomBorder - Canvas
        
        stackPanel.Children.Add(_uiCanvasManager.CreateShapeComboBox());
        stackPanel.Children.Add(scrollViewer);
        //stackPanel.Children.Add(canvas);
        
        return stackPanel;
    }
    
    private void Canvas_PointerPressed(object? sender, PointerPressedEventArgs e)
    {
        var startPoint = e.GetPosition(sender as Canvas);
        AddObjectToCanvas(startPoint);
    }
    
    private void AddObjectToCanvas(Point startPoint)
    {
        if (CurrentObject is null) return;
        Canvas.SetLeft(CurrentObject, Math.Round(startPoint.X, 0));
        Canvas.SetTop(CurrentObject, Math.Round(startPoint.Y, 0));
        Canvas.Children.Add(CurrentObject);
        CurrentObject = null;
    }
}