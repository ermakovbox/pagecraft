using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using PageCraft.Resources.Services;

namespace PageCraft
{
    public partial class MainWindow : Window
    {
        private int CellWidth { get; set; }

        private TextBox _widthCanvasTextBox = null!;
        private TextBox _heightCanvasTextBox = null!;
        private static Canvas? _currentCanvas;
        
        public MainWindow()
        {
            InitializeComponentCanvas();

            DataContext = this;
        }

        private void InitializeComponentCanvas()
        {
            AvaloniaXamlLoader.Load(this);

            _widthCanvasTextBox = this.FindControl<TextBox>("WidthCanvasTextBox") ?? throw new NullReferenceException();
            _heightCanvasTextBox = this.FindControl<TextBox>("HeightCanvasTextBox") ?? throw new NullReferenceException();
            var createCanvasButton = this.FindControl<Button>("CreateCanvasButton");
            if (createCanvasButton == null) return;
            createCanvasButton.Click += CreateNewCanvas;
        }

        private void ExportToHtmlButton_Click(object sender, RoutedEventArgs e)
        {
            if (_currentCanvas is null) return;
            var htmlElements = new List<string>();
            foreach (var control in _currentCanvas.Children)
            {
                string html;
                switch (control)
                {
                    case Rectangle rectangle:
                        var positionLeft = Math.Round(Canvas.GetLeft(rectangle));
                        var positionTop = Math.Round(Canvas.GetTop(rectangle));
                        html =
                            $"<div style='position: absolute;'><div style='width: {rectangle.Width}px; height: {rectangle.Height}px; background-color: {rectangle.Fill}; border: 2px solid {rectangle.Stroke}; position: relative; top: {positionTop}px; left: {positionLeft}px'></div></div>";
                        htmlElements.Add(html);
                        break;
                    case Line { Name: "VerticalLine" } line:
                        html =
                            $"<hr style='position: absolute; border: none; width: {line.StartPoint.X}px; height: {_currentCanvas.Height}px; border-right: {line.StrokeThickness}px solid {line.Stroke}; margin-block-start: 0; margin-inline-start: 0; margin-inline-end: 0;' />";
                        htmlElements.Add(html);
                        break;
                    case Line { Name: "HorizontalLine" } line:
                        html =
                            $"<hr style='position: absolute; border: none; width: {_currentCanvas.Width}px; height: {line.StartPoint.Y}px; border-bottom: {line.StrokeThickness}px solid {line.Stroke}; margin-block-start: 0; margin-inline-start: 0; margin-inline-end: 0;' />";
                        htmlElements.Add(html);
                        break;
                }
            }

            var htmlParent =
                $"<div style='width: {_currentCanvas.Width}px; height: {_currentCanvas.Height}px; background-color: {_currentCanvas.Background}; margin: auto;'>{htmlElements.Aggregate(string.Empty, (current, html) => current + html)}</div>";

            File.WriteAllText("rectangle.html", htmlParent);
            OpenBrowser("rectangle.html");
        }

        private void OpenBrowser(string fileName)
        {
            try
            {
                var processStartInfo = new System.Diagnostics.ProcessStartInfo
                {
                    FileName = fileName,
                    UseShellExecute = true
                };

                System.Diagnostics.Process.Start(processStartInfo);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening browser: {ex.Message}");
            }
        }

        private void CreateNewCanvas(object? sender, RoutedEventArgs e)
        {
            // Парсим значения ширины и высоты из TextBox
            if (int.TryParse(_widthCanvasTextBox.Text, out var canvasWidth) &&
                int.TryParse(_heightCanvasTextBox.Text, out var canvasHeight))
            {
                // Добавляем Canvas к содержимому ParentStackPanel
                var stackPanel = this.FindControl<StackPanel>("ParentStackPanel");

                if (stackPanel != null)
                {
                    var canvasManager = new CanvasManager(stackPanel, "DrawingCanvas", canvasWidth, canvasHeight,
                        new UiCanvasManager());

                    _currentCanvas = CanvasManager.Canvas;
                }

                if (_currentCanvas != null) DrawBootstrapGrid(_currentCanvas);
            }
            else
            {
                // Обработка ошибок ввода
            }
        }
        
        private void ResetCanvas(Canvas canvas)
        {
            var stackPanel = this.FindControl<StackPanel>("ParentStackPanel");
            stackPanel?.Children.Remove(canvas);
        }

        private void DrawBootstrapGrid(Canvas canvas, int columns = 12)
        {
            CellWidth = (int)(canvas.Width / columns);

            for (var i = 1; i < columns; i++)
            {
                var verticalLine = new Line
                {
                    Name = "VerticalLine",
                    StartPoint = new Point(CellWidth * i, 0),
                    EndPoint = new Point(CellWidth * i, canvas.Height),
                    Stroke = Brushes.Black,
                    StrokeThickness = 1,
                };

                var horizontalLine = new Line
                {
                    Name = "HorizontalLine",
                    StartPoint = new Point(0, CellWidth * i),
                    EndPoint = new Point(canvas.Width, CellWidth * i),
                    Stroke = Brushes.Black,
                    StrokeThickness = 1,
                };

                canvas.Children.Add(horizontalLine);
                canvas.Children.Add(verticalLine);
            }
        }
        
        private void OpenNewWindowButton_Click(object sender, RoutedEventArgs e)
        {
            var windowService = new WindowService();
            windowService.ShowPanAndZoomWindow();
            this.Close(); // Закрываем текущее окно
        }
    }
}